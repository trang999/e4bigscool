﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E4BigSchool.Startup))]
namespace E4BigSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
